import { ComposantModel } from "../services/mongo.service";
import { Composant } from "../models/composant.model";

export class composantService {
    public async GetComposantbyId(comp: Composant) {
        const res = await ComposantModel.findById(comp.id_composant)

        return res
    }

    public async GetAllComposants() {
        const res = await ComposantModel.find()

        return res
    }

    public async InsertComposant(comp: Composant) {
        var composant = new ComposantModel(comp)

        composant.save()
    }

    public async UpdateComposant(comp: Composant) {
        const filter = { _id: comp.id_composant}
        const update = {
            name: comp.name,
            url: comp.url,
            description: comp.description,
            update_time: Date.now()
        }

        await ComposantModel.findOneAndUpdate(filter, update)
    }

    public async DeleteComposantById(comp: Composant) {
        await ComposantModel.findByIdAndDelete(comp.id_composant)
    }
}