import { connect, Schema, model } from 'mongoose';
import { Composant } from '../models/composant.model';

const config = require('../config')

const mongoDb = connect(config.mongo.server + config.mongo.database);

const ComposantSchema = new Schema<Composant>({
    name: { type: String, required: true },
    url: { type: String, required: true },
    description: { type: String, required: true },
    creation_time: { type: Date, default: Date.now },
    update_time: { type: Date, default: Date.now },
})

export const ComposantModel = model<Composant>('Composant', ComposantSchema)
