import { Error400 } from "../errors/errors";

export class Composant {
    id_composant: string
    name: string
    url: string
    description: string
    creation_time: Date
    update_time: Date
    

    public constructor(init?: Partial<Composant>) {
        Object.assign(this, init);
    }

    public IsIdComposantValid(): boolean {
        if (this.id_composant === undefined) {
            throw new Error400("Missing argument, 'id_composant' can not be NULL")
        }
        if (this.id_composant === "") {
            throw new Error400("Empty argument, 'id_composant' can not be EMPTY")
        }
        return true
    }


    public IsNameValid(): boolean {
        if (this.name === undefined) {
            throw new Error400("Missing argument, 'name' can not be NULL")
        }
        if (this.name === "") {
            throw new Error400("Empty argument, 'name' can not be EMPTY")
        }
        return true
    }

    public IsUrlValid(): boolean {
        if (this.url === undefined) {
            throw new Error400("Missing argument, 'url' can not be NULL")
        }
        if (this.url === "") {
            throw new Error400("Empty argument, 'url' can not be EMPTY")
        }
        return true
    }

    public IsDescriptionValid(): boolean {
        if (this.description === undefined) {
            throw new Error400("Missing argument, 'description' can not be NULL")
        }
        if (this.description === "") {
            throw new Error400("Empty argument, 'description' can not be EMPTY")
        }
        return true
    }
}