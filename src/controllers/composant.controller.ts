import { Router } from 'express';
import { composantService } from '../datas/composant.data';
import { Composant } from '../models/composant.model';
import { returnSuccess, returnCreated, returnUpdated, returnDeleted } from '../errors/success';

const composantController = Router();
const composantSvc = new composantService()

composantController.get('/get/:id_composant', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const comp: Composant = new Composant({
            id_composant: req.params.id_composant
        })

        comp.IsIdComposantValid()

        const comp_res = await composantSvc.GetComposantbyId(comp)

        const message: string = "Get composant by id"
        returnSuccess(res, comp_res, message)

    } catch (err) {
        next(err)
    }
})

composantController.get('/all', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const comp_res = await composantSvc.GetAllComposants()

        const message: string = "Get all composants"
        returnSuccess(res, comp_res, message)

    } catch (err) {
        next(err)
    }
})

composantController.post('/add', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const comp: Composant = new Composant({
            name: req.body.name,
            url: req.body.url,
            description: req.body.description,
        })

        comp.IsNameValid()
        comp.IsUrlValid()
        comp.IsDescriptionValid()

        await composantSvc.InsertComposant(comp)

        const message: string = "Add composant"
        returnCreated(res, message)

    } catch (err) {
        next(err)
    }
})

composantController.put('/update', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const comp: Composant = new Composant({
            id_composant: req.body.id_composant,
            name: req.body.name,
            url: req.body.url,
            description: req.body.description,
        })

        comp.IsIdComposantValid()

        await composantSvc.UpdateComposant(comp)

        const message: string = "Update composant by id"
        returnUpdated(res, message)

    } catch (err) {
        next(err)
    }
})

composantController.delete('/delete', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const comp: Composant = new Composant({
            id_composant: req.body.id_composant,
        })

        comp.IsIdComposantValid()

        await composantSvc.DeleteComposantById(comp)

        const message: string = "Delete composant by id"
        returnDeleted(res, message)

    } catch (err) {
        next(err)
    }
})

export { composantController }